from CoderInterface import CoderInterface


class MyCoder(CoderInterface):
    def run(self, coder_info, string_to_process):
        if coder_info == 'code':
            return self._code(string_to_process)
        if coder_info == 'decode':
            return self._decode(string_to_process)

    def _code(self, string_to_code):
        key = 3
        offset = 'r'
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        cipher = {}
        for i in range(len(alphabet)):
            if offset == "r":
                shifted_index = (i + key) % len(alphabet)
            elif offset == "l":
                shifted_index = (i - key) % len(alphabet)
            else:
                raise ValueError("Invalid offset")
            cipher[alphabet[i]] = alphabet[shifted_index]

        result = ""
        for symbol in string_to_code:
            encrypted_symbol = cipher.get(symbol, symbol)
            result += encrypted_symbol
        return result

    def _decode(self, string_to_code):
        return self._code(string_to_code)
