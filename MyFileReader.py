from FileReaderInterface import FileReaderInterface


class CustomFileGenerator:
    def __init__(self, file_name, buffer_size):
        self.file_name = file_name
        self.buffer_size = buffer_size
        self.file = None

    def __iter__(self):
        with open(self.file_name, 'r') as file:
            while True:
                info = file.read(self.buffer_size)
                if not info:
                    break
                yield info

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


class MyFileReader(FileReaderInterface):
    def read_file(self, file_path, buffer_size):
        return CustomFileGenerator(file_path, int(buffer_size))
