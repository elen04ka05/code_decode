from ConfigReaderInterface import ConfigReaderInterface
from ConfigException import ConfigException


class MyConfigReader(ConfigReaderInterface):
    def read_config(self, file_path):
        config_data = {}

        try:
            with open(file_path, 'r') as file:
                for line in file:
                    key, value = line.strip().split('=', 1)
                    config_data[key.strip()] = value.strip()
            if 'buffer_size' in config_data:
                try:
                    config_data['buffer_size'] = int(config_data['buffer_size'])
                except ValueError:
                    raise ConfigException("Buffer size must be an integer")
        except FileNotFoundError:
            raise ConfigException("Config file not found")
        except Exception as e:
            raise ConfigException("Error reading config file: {}".format(str(e)))

        return config_data
